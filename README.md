# Vulkan Renderer

## Description
This is a simple C++ renderer using Vulkan based on the https://vulkan-tutorial.com/Development_environment.

## Installation
Make sure to install the Vulkan SDK. For more info https://vulkan-tutorial.com/Development_environment

## Authors and acknowledgment
https://github.com/Overv/VulkanTutorial

## License
https://creativecommons.org/publicdomain/zero/1.0/